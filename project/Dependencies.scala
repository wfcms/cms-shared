import sbt._
import xyz.whynotzoidberg.dependencymanagement.DependencyManagement.DependencyTypeDef.{Dependency, Group, JavaDep}
import xyz.whynotzoidberg.dependencymanagement.DependencyManagement.Implicits._

object Dependencies {
	object JUnit {
		object Jupiter extends Group("org.junit.jupiter") {
			override def artifacts: Seq[Dependency] = Seq(API, Engine)

			val version = "5.0.1"
			object API extends JavaDep("junit-jupiter-api", version, Test)
			object Engine extends JavaDep("junit-jupiter-engine", version, Test)
		}

		object Vintage extends Group("org.junit.vintage") {
			override def artifacts: Seq[Dependency] = Seq(Engine)

			object Engine extends JavaDep("junit-vintage-engine", "4.12.1", Test)
		}

		object Platform extends Group("org.junit.platform") {
			override def artifacts: Seq[Dependency] = Seq(Launcher, Runner)

			val version = "1.0.1"
			object Launcher extends JavaDep("junit-platform-launcher", version, Test)
			object Runner extends JavaDep("junit-platform-runner", version, Test)
		}

		def groups: Seq[Group] = Seq(Jupiter, Vintage, Platform)
	}
}